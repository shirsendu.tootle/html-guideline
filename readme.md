# HTML Coding Guidelines

## Document Structure

1. **Document Type:** Use HTML5 (`<!DOCTYPE html>`) for all projects.
2. **Language Declaration:** Include the `lang` attribute in the `<html>` tag to specify the document's primary language (e.g., `en` for English).

```html
<!DOCTYPE html>
<html lang="en">
```

3. **Character Encoding:** Specify the character encoding with the `<meta>` tag in the `<head>` section.

```html
<meta charset="UTF-8">
```

4. **Viewport Meta Tag:** Include a viewport meta tag for responsive design.

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```

## Indentation and Formatting

1. Use **four spaces** for indentation (no tabs).
2. Maintain consistent and readable formatting throughout the document.
3. Keep line lengths under **80 characters** for readability and version control.

## Comments

1. Use **descriptive comments** to explain complex code, sections, or any deviations from the norm.
2. Remove unnecessary or redundant comments in production code.

```html
<!-- This is a descriptive comment explaining the purpose of the following code. -->
```

## Naming Conventions

1. Use **semantic and descriptive names** for IDs, classes, and file names.
2. Avoid using generic or cryptic names (e.g., `div1`, `mytext`).

```html
<div id="header" class="main-header">
```

## Semantics

1. Follow HTML5 semantics when structuring your documents.
2. Use appropriate HTML elements (e.g., `<header>`, `<nav>`, `<section>`, `<article>`, `<aside>`, `<footer>`) to improve accessibility and SEO.

```html
<header>
  <h1>Page Title</h1>
  <nav>
    <ul>
      <li><a href="/">Home</a></li>
      <li><a href="/about">About</a></li>
    </ul>
  </nav>
</header>
```

## Validation

1. Ensure your HTML code is **W3C compliant** by validating it using online tools like the W3C Markup Validation Service (https://validator.w3.org/).

## Accessibility

1. Prioritize **web accessibility** by providing alternative text for images, using semantic elements, and maintaining proper document structure.

## External Resources

1. Include external resources (e.g., CSS, JavaScript) with clear, concise paths.

```html
<link rel="stylesheet" href="styles.css">
<script src="script.js"></script>
```


## Testing

1. **Test your HTML** in multiple browsers and devices to ensure cross-browser compatibility.
2. Validate forms and user interactions to guarantee functionality.


## General Points

1. Do not add inline css
2. Page will be used for PDF so make the HTML PDF friendly
3. Avoid using multiple third party libraries which might raise issue for loading the PDF
4. Should not break in different screens
5. Make the graphics smaller and compressed without effecting the quality to keep the PDF size low

---
